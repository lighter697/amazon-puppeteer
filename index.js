const express = require('express');
const puppeteer = require('puppeteer');
const fs = require('fs');
const app = express();
const UserAgent = require('user-agents');
const filters = {deviceCategory: 'desktop'};

let options = {
    headless: true,
    ignoreHTTPSErrors: true,
    defaultViewport: {width: 2560, height: 1440}
};

const preloadFile = fs.readFileSync('./preload.js', 'utf8');

app.get('/', (req, res) => {
    res.send('<h1>It works!</h1>');
});

app.get('/request/:url', async (req, res) => {

    let args = [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-infobars',
        '--window-position=0,0',
        '--ignore-certificate-errors',
        '--ignore-certificate-errors-spki-list',
    ];


    if (req.query.proxy) {
        console.log(req.query.proxy);
        args = [...args, '--proxy-server=' + req.query.proxy];
    }
    try {
        const userAgent = new UserAgent(filters);
        console.log(userAgent.data.userAgent);
        const browser = await puppeteer.launch({...options, args: args});
        const page = await browser.newPage();
        if (req.query.proxy) {
            await page.authenticate({
                username: process.env.PROXY_USERNAME,
                password: process.env.PROXY_PASSWORD,
            });
        }
        await page.setUserAgent(userAgent.data.userAgent);
        await page.evaluateOnNewDocument(preloadFile);
        await page.goto(req.params.url, {waitUntil: 'load', timeout: 0});
        const bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
        await browser.close();
        res.send(bodyHTML);
    } catch (e) {
        console.log(e.message);
        res.status(130).send('error');
    }

});

app.get('/product/:url', async (req, res) => {

    let args = [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-infobars',
        '--window-position=0,0',
        '--ignore-certifcate-errors',
        '--ignore-certifcate-errors-spki-list',
    ];

    if (req.query.proxy) {
        console.log(req.query.proxy);
        args = [...args, '--proxy-server=' + req.query.proxy];
    }
    try {
        const userAgent = new UserAgent(filters);
        console.log(userAgent.data.userAgent);
        const browser = await puppeteer.launch({...options, args: args});
        const page = await browser.newPage();
        if (req.query.proxy) {
            await page.authenticate({
                username: process.env.PROXY_USERNAME,
                password: process.env.PROXY_PASSWORD,
            });
        }
        await page.setUserAgent(userAgent.data.userAgent);
        await page.evaluateOnNewDocument(preloadFile);
        await page.goto(req.params.url, {waitUntil: 'load', timeout: 0});

        await page.evaluate(() => {
            const elements = [...document.querySelectorAll('input[name="submit.addToCart"]')];
            if (elements.length) {
                elements[0].click();
            }
        });
        await page.waitForNavigation();
        const bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
        await browser.close();
        res.send(bodyHTML);
    } catch (e) {
        console.log(e.message);
        res.status(130).send('error');
    }
});

app.listen(4000, function () {
    console.log('Example app listening on port 4000!');
});
