const puppeteer = require('puppeteer');
const UserAgent = require('user-agents');
const fs = require('fs');


(async () => {
    const filters = {deviceCategory: 'desktop'};
    const userAgent = new UserAgent(filters);
    const args = [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-infobars',
        '--window-position=0,0',
        '--ignore-certifcate-errors',
        '--ignore-certifcate-errors-spki-list',
        '--user-agent="' + userAgent.data.userAgent + '"'
    ];

    const options = {
        args,
        headless: true,
        ignoreHTTPSErrors: true,
        // userDataDir: './tmp',
    };

    const browser = await puppeteer.launch(options);
    const page = await browser.newPage();
    const preloadFile = fs.readFileSync('./preload.js', 'utf8');
    await page.evaluateOnNewDocument(preloadFile);
    await page.goto('https://intoli.com/blog/not-possible-to-block-chrome-headless/chrome-headless-test.html');
    await page.screenshot({path: 'chrome-headless-test.png'});
    await browser.close();
})();
